+++
title = "Meetup 12 de Julio 2019"
publishdate = "2019-07-12"
date = "2019-07-12"
event_place = "Laboratorio de electrónica, FEI"
description = "El evento de julio, en el ahora laboratorio de electrónica"
thumbnail = "/img/eventos/meetup-2019-07-12.jpg"
author = "categulario"
turnout = 15
draft = false
+++

## Las platicas

1. Historia de un proyecto que falló mucho por Jail Andrade
2. Tics Xalapa por Álvaro García
3. Presentación de Open Hip Hop por Isidro Hernández

## Sobre la sede

El **laboratorio de electrónica** es el nuevo nombre del laboratorio de redes, en la Facultad de Estadística e Informática.

## Sobre el evento

Los meetups de xalapacode son encuentros en los que se realizan una serie de desconferencias de 15 minutos de duración por miembros de la comunidad como tu sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
