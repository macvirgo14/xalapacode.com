+++
title = "Meetup 14 de Diciembre 2018"
publishdate = "2018-12-25"
date = "2018-12-14"
event_place = "Fonda La Independencia"
description = "Último meetup del año 2018"
thumbnail = "/img/eventos/meetup-2018-12-14.jpg"
topics = ["Kanban", "Firebase", "User Experience", "CSS Grid", "Stack Overflow"]
turnout = 35
draft = false
+++

Último meetup del año celebrado en la fondita *la independencia*.

## Temas

* firebase (Blanca Malerva)
* UX [@roccons](https://twitter.com/roccons)
* css grid (Rafael Fernández)
* stack overflow [@categulario](https://twitter.com/categulario)
* Kanban (Johnathan)
