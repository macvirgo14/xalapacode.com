+++
title = "Meetup 19 de Octubre 2018"
publishdate = "2019-01-23"
date = "2018-10-19"
event_place = "Sada de maestros FEI UV"
description = "Meetup de octubre"
thumbnail = "/img/eventos/meetup-2018-10-19.jpg"
topics = ["Graph QL", "Algoritmos evolutivos"]
turnout = 12
+++

En esta ocasión la sede fue la sala de maestros de la FEI UV.

## Temas

* GraphQL [@categulario](https://twitter.com/categulario)
* Algoritmos Evolutivos Multiobjetivo (Brenda Laura Vázquez)
